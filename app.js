const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const app = express();
//cookie parser

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 3000;

// mongodb connection
//mongoose.connect('mongodb://localhost:27017/recipedex');
mongoose.connect('mongodb://rod:1234@ds127993.mlab.com:27993/recipedex');
var db = mongoose.connection;
// mongo error
db.on('error', console.error.bind(console, 'connection error:'));

// use sessions for tracking logins
app.use(session({
  secret: 'recipedex loves you',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// make user ID available in templates
app.use(function (req, res, next) {
  res.locals.currentUser = req.session.userId;
  res.locals.name = req.session.name;
  //console.log(req.session.name);
  next();
});

// parse incoming requests
app.use(bodyParser.urlencoded({ extended: false}))

//view engine
app.set('view engine', 'pug');

//static assets
app.use(express.static('public'));

//middleware
app.use((req, res, next) => {
  next();
});

//include routes
const routes = require('./routes/index');
app.use('/', routes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// listen on port 3000
app.listen(port, () => {
  console.log('the app is running on localhost: 3000');
});