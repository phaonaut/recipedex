const express = require('express');
const router = express.Router();
const unirest = require('unirest');
const User = require('../models/user');
const mid = require('../middleware');

// GET routes this is the login route
router.get('/', mid.loggedOut, (req, res) => {
  res.render('login');
});

// POST login route
router.post('/', (req, res, next) => {
  if (req.body.email && req.body.password) {
    User.authenticate(req.body.email, req.body.password, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password');
        err.status = 401;
        return next(err);
      } else {
        req.session.userId = user._id;
        req.session.name = user.name;
        return res.redirect('/dashboard');
      }
    });
  } else {
    var err = new Error('Email and password are required');
    err.status = 401;
    return  next(err);
  }  
});

// GET /logout
router.get('/logout', function(req, res, next) {
  if (req.session) {
    //delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

// GET /signup
router.get('/signup', mid.loggedOut, (req, res) => {
  res.render('signup');
});

// POST /signup
router.post('/signup', (req, res, next) => {
  if (req.body.email &&
    req.body.name &&
    req.body.password &&
    req.body.confirmPassword) {
    //confirm passwords match
    if (req.body.password !== req.body.confirmPassword) {
      var err = new Error('passwords do not match');
      err.status = 400;
      return next(err);
    }
    //create object with form input 
    var userData = {
      email: req.body.email,
      name: req.body.name,
      favoriteBook: req.body.favoriteBook,
      password: req.body.password
    };

    //user schemas 'create' method to insert doc into mongo
    User.create(userData, function(error, user) {
      if (error) {
        return next(error);
      } else {
        req.session.userId = user._id;
        req.session.name = user.name;
        return res.redirect('/dashboard');
      }
    });

  } else {
    var err = new Error('All fields required');
    err.status = 400;
    return next(err);
  }
});

// GET /dashboard
router.get('/dashboard', mid.requiresLogin, (req, res, next) => {
  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        return next(error);
      } else {
        return res.render('dashboard', { name: user.name });
      }
    });
});

// GET /random - food2fork call but
router.get('/random', (req, res) => {
  let randomNumber = Math.floor(Math.random() * 15000);
  
  //add random num generator to pass into unirest
  unirest.get("https://community-food2fork.p.mashape.com/get?key=b13530326ea5e55db52534bd713d923b&rId="+randomNumber)
  .header("X-Mashape-Key", "H9EPigTpiYmsh4tY8Ot6l7zSUIEkp1NH6wWjsnBZsPGyb2P1gR")
  .header("Accept", "application/json")
  .end(function (result) {

    result = JSON.parse(result.body); 
    let randomRecipe = result.recipe; 
    //console.log(randomRecipe);
    res.render('recipe', {randomRecipe});
  });
});

// GET /search
router.get('/search', (req, res) => {
  //console.log(req.query.q);
  let query =  req.query.q;
  if(req.query.q) {
    unirest.get("https://community-food2fork.p.mashape.com/search?key=b13530326ea5e55db52534bd713d923b&rId=37852&q="+ query)
    .header("X-Mashape-Key", "H9EPigTpiYmsh4tY8Ot6l7zSUIEkp1NH6wWjsnBZsPGyb2P1gR")
    .header("Accept", "application/json")
    .end(function (result) {
      result = JSON.parse(result.body);
      let recipes = result.recipes; 

      res.render('search', {recipes});
    });
  } else {
    res.render('search');
  }
});

// GET /:id - routes id us used for api call
router.get('/:id', (req, res) => {
  //console.log(req.params.id);
  let id = req.params.id;

  unirest.get("https://community-food2fork.p.mashape.com/get?key=b13530326ea5e55db52534bd713d923b&rId="+ id)
  .header("X-Mashape-Key", "H9EPigTpiYmsh4tY8Ot6l7zSUIEkp1NH6wWjsnBZsPGyb2P1gR")
  .header("Accept", "application/json")
  .end(function (result) {
    result = JSON.parse(result.body);
    let randomRecipe = result.recipe; 

    if(randomRecipe !== null) {
      res.render('recipe', {randomRecipe});
    } else {
      res.redirect('/dashboard');
   }
   });
}); 

module.exports = router;